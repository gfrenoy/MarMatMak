// **************************************************************************
// Copyright (C) 2007 BxLUG - http://www.bxlug.be
// Please submit comments and suggestions to devel [�] lists.bxlug.be
// **************************************************************************
// This file is part of � MarMatMak �
// a software to easily create marketing material
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details:
//                 http://www.gnu.org/copyleft/gpl.html
// ***************************************************************************

var current_template_id = null;
var template_data = null;

function trim(string) {
return string.replace(/(^\s*)|(\s*$)/g,'');
} 
function getXMLHttpRequest() {
  var obj = null;
  if (window.XMLHttpRequest) obj = new XMLHttpRequest();
  else if (window.ActiveXObject) obj = new ActiveXObject("Microsoft.XMLHTTP");
  return obj;
}

function get_template_data(template_id) {
  // Data is already loaded, just return it
  if (template_id == current_template_id) return template_data;

  // Ask the server for data related to selected template
  var req = getXMLHttpRequest();
  req.open("GET", 'callback.php?fct=tdata&id='+escape(template_id), false);
  req.send(null);
  if (req.status == 200) {
    current_template_id = template_id;
    template_data = req.responseXML;
    return template_data;
  }

  // Ooops, something went wrong
  return null;
}

function change_template() {
  var tpl         = document.getElementById('tpl');
  var preview_img = document.getElementById('preview_img');
  var tpl_table   = document.getElementById('tpl_table');
  var tpl_desc    = document.getElementById('tpl_desc');
  var data        = tpl!=null?get_template_data(tpl.value):null;
  if (preview_img != null && tpl_table != null && data != null) {
    // Update preview
    var els = data.getElementsByTagName('preview');
    if (els != null && els.length == 1) {
      preview_img.src = els[0].getAttribute('src');
    } else {
      preview_img.src = 'images/error-template.png';
    }
    // Update description (if any)
    if (tpl_desc != null) {
      tpl_desc.style.display = 'none';
      els = data.getElementsByTagName('description');
      if (els != null && els.length == 1 && trim(els[0].textContent)!='') {
        tpl_desc.cells[1].innerHTML = els[0].textContent;
        tpl_desc.style.display = '';
      }
    }

    // Erase old texts
    for (var row=tpl_table.rows.length-1; row>=0; row--) {
      if (tpl_table.rows[row].id.substr(0, 8) == 'tpl_text') {
        tpl_table.deleteRow(row);
      }
    }

    // Create new row and label for each customizable text
    els = data.getElementsByTagName('text');
    for (var i=0; i<els.length; i++) {
      var newRow       = tpl_table.insertRow(tpl_table.rows.length-1);
      newRow.id        = 'tpl_text'+i;
      newRow.innerHTML = '<td align="right">' +
                         '  <label for="'+els[i].getAttribute('name')+'">'+els[i].getAttribute('label')+'</label>' +
                         '</td>' +
                         '<td>' +
                         '  <input name="'+els[i].getAttribute('name')+'" type="text" id="'+els[i].getAttribute('name')+'" value="' + els[i].textContent + '" size="30">' +
                         '</td>';
    }
  }
}

// $Id: main.js,v 1.2 2007-03-22 12:40:54 gfrenoy Exp $
