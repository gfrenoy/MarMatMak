<?php
// **************************************************************************
// Copyright (C) 2007 BxLUG - http://www.bxlug.be
// Please submit comments and suggestions to devel [�] lists.bxlug.be
// **************************************************************************
// This file is part of � MarMatMak �
// a software to easily create marketing material
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details:
//                 http://www.gnu.org/copyleft/gpl.html
// ***************************************************************************

//
// Dependencies
//

// Template management
require_once('./lib/templates.php');

// PDF library
require_once('./lib/fpdi/fpdi.php');

//
// Select template
//
$template = isset($_POST['tpl']) && isset($templates->list[$_POST['tpl']]) 
             ? $templates->list[$_POST['tpl']] : current($templates);

// ----------------------------------------------------------------
// Helper Functions
// ----------------------------------------------------------------

// Print a single card at given position in specified position
// using information in template
function printDocument($X, $Y, $template, &$pdf) {
  // Size of one document
  $rectW = $template->document_width; 
  $rectH = $template->document_height;

  // Set background
  $background_file = "templates/{$template->id}/{$template->background_img}";
  if (file_exists($background_file)) {
    $pinfo = pathinfo($background_file);
    if ($pinfo['extension'] == 'pdf') {
      // Background is a PDF
      $pdf->setSourceFile($background_file);
      $tplidx = $pdf->importPage(1);
      $pdf->useTemplate($tplidx, $X, $Y, $rectW, $rectH);
    }
    else {
      // Background is an JPG image
      $d = getimagesize($background_file);
      $pdf->Image($background_file, $X, $Y, $rectW, $rectH);
    }
  }

  // Draw outside border
  if ($template->draw_border) {
    $pdf->setLineWidth(0.12);
    $pdf->setDrawColor (0,0,0);
    $pdf->Line($X, $Y, $X+$rectW, $Y);
    $pdf->Line($X, ($Y+$rectH), $X+$rectW, ($Y+$rectH));
    $pdf->Line($X, $Y, $X, ($Y+$rectH));
    $pdf->Line($X+$rectW, $Y, $X+$rectW, ($Y+$rectH));
  }

  // Display customizable texts
  foreach($template->texts->children() as $text) {
    $attr = $text->attributes();
    $pdf->SetFontSize($attr['font_size']);
    $pdf->Text($X+$attr['x'], $Y+$attr['y'], $text->CDATA());
  }
}

$pdf = new FPDI();
$pdf->AddPage();
$pdf->setFont($template->font_family, $template->font_family_options);

// Extract information from interactive form and replace
// default template values
for ($i=0; $i<count($template->texts->text); $i++) {
  $attr = $template->texts->text[$i]->attributes();
  if (isset($_POST[$attr['name']])) {
    $template->texts->text[$i]->setCDATA(utf8_decode($_POST[$attr['name']]));
  }
}

// How many instances on the page ?
for ($col=1; $col<=$template->column_count; $col++) {
  for ($row=1; $row<=$template->row_count; $row++) {
    printDocument($template->left_margin+$template->document_width *($col-1),
                  $template->top_margin +$template->document_height*($row-1),
                  $template, $pdf);
  }
}

// Force download
$pdf->Output("marmatmak_{$template->id}.pdf", 'D'); 
?>
