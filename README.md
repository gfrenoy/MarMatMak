# MarMatMak - G�n�rateur de mat�riel promotionnel #

### � propos ###

MarMatMak est un outil qui permet de g�n�rer du mat�riel promotionnel � partir d'un mod�le pr�d�fini qui pr�voit un certain nombre de champs personnalisables par l'utilisateur.

Une fois les valeurs des champs choisies, l'utilisateur re�oit alors un fichier PDF personnalis�.

### Licence ###

Le code source de cet outil est disponible sous licence GPL. Nous serions tr�s heureux que vous examiniez et modifiez ce programme; n'h�sitez pas � nous faire parvenir vos am�liorations !

Pour obtenir la derni�re version des sources, rejoingnez le projet sur https://git.framasoft.org/bxlug/MarMatMak.

(c) 2007,2015 BxLUG - Groupe des Utilisateurs de GNU/Linux de Bruxelles 

### Cr�dits ###

Mainteneur: secretaire@bxlug.be

Contributeurs: Ga�tan Frenoy, Yannick Warnier

Le code se base sur FPDI de Setasign (Jan Slabon) lui m�me bas� sur FPDF d'Olivier Plathey.
Le code se base sur simplexml44 Version 0.4.4 de Ingo Schramm, Ister.ORG
