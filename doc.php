<?
// **************************************************************************
// Copyright (C) 2007 BxLUG - http://www.bxlug.be
// Please submit comments and suggestions to devel [à] lists.bxlug.be
// **************************************************************************
// This file is part of « MarMatMak »
// a software to easily create marketing material
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details:
//                 http://www.gnu.org/copyleft/gpl.html
// ***************************************************************************

// Overrides server configuration and force charset
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Générateur de matériel promotionnel</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="http://www.bxlug.be/css/faty.css" media="screen" title="Faty" />
</head>

<body>
<div id="top">
  <h1 id="bxlug"><span>BxLUG</span> groupe des utilisateurs de GNU/Linux de Bruxelles</h1>
</div>

<div id="main-content">

  <h1>Documentation du « Générateur de matériel promotionnel »</h1>

  <h2>À propos</h2>
    <p>Suite à une discussion sur la liste de distribution du BxLUG et sous l'impulsion de <a href="http://www.bxlug.be/identities/132"
    title="La page de Philip Richardson">Philip Richardson</a>, l'idée d'avoir un outil qui permette de créer rapidement
    des documents promotionnels a germé dans nos têtes.</p>
    <p>En se basant sur le code de Jean-Sébastien Rousseau-Piot qui avait réalisé un <a href="http://www.reseaucitoyen.be/cartevisite/"
    title="Générateur de carte personnalisable pour Réseau Citoyen">générateur de carte personnalisable pour le Réseau Citoyen</a>,
    <a href="http://www.bxlug.be/identities/113" title="Page Yannick Warnier">Yannick Warnier</a> et 
    <a href="http://www.bxlug.be/identities/22" title="Page de Gaëtan Frenoy">Gaëtan Frenoy</a> ont retroussé leurs manches et
    démarré un petit projet pour répondre au besoin exprimé par le BxLUG.</p>

  <h2>Utilisation</h2>
    <p>Elle se veut très simple :
      <ol>
        <li>Choisissez le modèle dans la liste proposée</li>
        <li>Remplissez les champs affichés.  Chaque modèle peut avoir un nombre de champ différent</li>
        <li>Appuyez sur « Créer le document »</li>
      </ol>
      L'outil va alors reprendre le modèle choisi mais en y ajoutant, à l'endroit spécifié par son
      auteur, les textes choisis par l'utilisateur.</p>

  <h2>Création de nouveaux modèles</h2>
    <h3>Idée générale</h3>
    <p>L'outil doit permettre un ajout très rapide et aisé de nouveaux modèles.  Aucune connaissance en programmation n'est
    nécessaire pour créer un modèle.  Il faut savoir créer des documents graphiques et éditer un fichier texte.</p>

    <h3>Le répertoire « templates »</h3>
    <p>Sur le serveur hébergeant l'outil, un répertoire « templates » doit exister.  Ce répertoire contient lui-même une série de sous-répertoires qui contiennent la définition et les images de base d'un modèle.</p>
    <p>Le nom du sous-répertoire sera l'identifiant du modèle, il doit être composé uniquement de lettres, chiffres ou du signe « - ».</p>

    <h3>Le fichier « config.xml »</h3>
    <p>Chaque sous-répertoire doit contenir un fichier nommé « config.xml ».  Si ce fichier n'est pas présent, le modèle ne sera pas pris en compte du tout.</p>
    <p>Le ficher « config.xml » est un fichier XML valide.</p>
    <p>L'exemple ci-dessous montre en gras les valeurs à modifier pour votre modèle.</p>
    <pre>
&lt;?xml version="1.0" encoding="UTF-8" standalone="yes"?&gt;
&lt;marmatmak xmlns="http://www.bxlug.be/ns/marmatmak"&gt;
  &lt;name&gt;<b>Le nom de votre modèle</b>&lt;/name&gt;
  &lt;description&gt;&lt;<b>![CDATA[&amp;copy; 2007 - Réalisé par Serge Smeesters]]&gt;</b>&lt;/description&gt;
  &lt;document lmargin="<b>50</b>" tmargin="<b>80</b>" width="<b>80</b>" height="<b>50</b>" rows="<b>5</b>" columns="<b>2</b>"/&gt;
  &lt;background src="<b>background.jpg</b>"/&gt;
  &lt;preview    src="<b>preview.jpg</b>"/&gt;
  &lt;texts&gt;
    &lt;text name="<b>param1</b>" x="<b>7</b>" y="<b>24</b>" font_size="<b>9</b>" label="<b>Le premier paramètre</b>"/&gt;
    &lt;text name="<b>param2</b>" x="<b>7</b>" y="<b>28</b>" font_size="<b>9</b>" label="<b>Le 2e paramètre</b>"/&gt;
    &lt;text name="<b>param3</b>" x="<b>7</b>" y="<b>33</b>" font_size="<b>8</b>" label="<b>Le 3e paramètre</b>"/&gt;
  &lt;/texts&gt;
&lt;/marmatmak&gt;
    </pre>
    <p>Le noeud principal doit être nommé « marmatmak ».</p>
    <p>Le noeud « name » contient le nom du modèle qui apparaîtra dans la liste des modèles proposés par l'outil.</p>
    <p>Le noeud « description » contient une description du modèle.  Elle sera affichée dans le formulaire interactif proposé à l'utilisateur.  La description peut contenir des balises HTML, il est alors recommandé d'utiliser une <a href="http://en.wikipedia.org/wiki/CDATA" title="Explications CDATA">section CDATA.</a></p>
    <p>Le noeud « document » définit la taille du document et le nombre d'instance de ce document que vous voulez générer sur une page.  Une page est toujours au format A4 et orientation « portrait » (210mm x 297mm).  Le noeud « document » comporte une série d'attributs:
      <ul>
        <li>« lmargin » définit la marge à gauche,</li>
        <li>« tmargin » définit la marge du haut,</li>
        <li>« width » définit la largeur d'un document (en millimètres),</li>
        <li>« height » définit la hauteur d'un document (en millimètres),</li>
        <li>« rows » définit le nombre de document à créer sur une hauteur de page (ie nombre de lignes),</li>
        <li>« columns » définit le nombre de document à créer sur une largeur de page (ie nombre de colonnes).</li>
      </ul>
    </p>
    <p>Le noeud « background » définit le fond du document.  La taille de l'image sera automatiquement adaptée à la taille du document.  Le format du fichier doit être <a title="Explication format JPEG" href="http://fr.wikipedia.org/wiki/JPEG">JPEG</a>, <a title="Explication format PNG" href="http://fr.wikipedia.org/wiki/Portable_Network_Graphics">PNG</a> ou <a title="Explication format PDF" href="http://fr.wikipedia.org/wiki/PDF">PDF</a>.</p>
    <p>Le noeud « preview » définit l'image à afficher lors du choix du modèle par l'utilisateur.  Cette image permet à l'utilisateur de se faire une idée sur le résultat.  La taille du fichier ne doit pas être trop élevée (~40 KB) pour que la mise à jour ne soit pas trop lente.  Le format du fichier n'a pas d'importance mais doit pouvoir être interprété par un navigateur.</p>
    <p>Le noeud « texts » définit l'ensemble des textes modifiables par l'utilisateur.  Chaque modèle doit définir au moins un texte.  Le nombre maximum de textes n'est pas déterminé.  Chaque texte est définit par un sous-noeud « text ».  Un sous-noeud « text » comporte les attributs suivants:
      <ul>
        <li>« name » définit le nom interne du texte</li>
          <li>« x » définit la position horizontale du texte sur le document (en millimètres).  La position x=0 correspond au bord gauche du document.</li>
          <li>« y » définit la position verticale du texte sur le document (en millimètres).  La position y=0 correspond au bord supérieur du document.</li>
          <li>« font_size » définit la taille de caractère à utiliser pour ce texte.</li>
          <li>« label » définit le libellé du texte dans le formulaire à remplir par l'utilisateur.</li>
      </ul>
      Chaque sous-noeud « text » correspond à une entrée du formulaire interactif qui sera proposé à l'utilisateur.
    </p>

  <h2>Code source et licence</h2>
    <p>Le code source de cet outil est disponible sous <a href="http://www.gnu.org/licenses/gpl.html">licence GPL version 3</a>.  Nous serions très heureux que vous examiniez et modifiez ce programme;  n'hésitez pas à nous faire parvenir vos améliorations !</p>
    <p>Pour obtenir la dernière version des sources, rendez-vous sur <a href="https://git.framasoft.org/bxlug/MarMatMak">https://git.framasoft.org/bxlug/MarMatMak</a>.</p>

  <h2>Crédits</h2>
    <p>Le code se base sur FPDI de Setasign (Jan Slabon) lui même basé sur FPDF d'Olivier Plathey.</p>
    <p>Le code se base sur simplexml44 Version 0.4.4 de Ingo Schramm, Ister.ORG</p>
</div>

<div id="footer">
<p>&copy; 2007 BxLUG - Groupe des Utilisateurs de GNU/Linux de Bruxelles
</div>
</body>
</html>
